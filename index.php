<?php

require_once("animal.php");
require_once("ape.php");
require_once("frog.php");

$sheep = new Animal("shaun");
echo "Name : " . $sheep->name;
echo "<br>";
echo "Legs : " . $sheep->legs;
echo "<br>";
echo "Cold Blooded : " . $sheep->cold_blooded;
echo "<br>";
echo "<br>";

$frog = new Frog("buduk");
echo "Name : " . $frog->name;
echo "<br>";
echo "Legs : " . $frog->legs;
echo "<br>";
echo "Cold Blooded : " . $frog->cold_blooded;
echo "<br>";
echo "Jump : ";
echo $frog->jump();
echo "<br>";
echo "<br>";

$ape = new Ape("kera sakti");
echo "Name : " . $ape->name;
echo "<br>";
echo "Legs : " . $ape->legs;
echo "<br>";
echo "Cold Blooded : " . $ape->cold_blooded;
echo "<br>";
echo "Yell : ";
echo $ape->yell();
echo "<br>";
echo "<br>";

?>